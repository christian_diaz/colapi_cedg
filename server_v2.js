var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./users2.json');
var URLbase = "/colapi/v4/";
var baseMlabURL = "https://api.mlab.com/api/1/databases/colapidb_cedg/collections/";
var apiKey = "apiKey=8I89WCD5GQOSapHVuElAZYj-TTFNZc9R";

app.listen(port);
app.use(bodyParser.json());
console.log("colapi escuchando en puerto " + port + "...");

//logout desde polymer a mLab ***************************************************

//POST logout usando API mLab para conexion con MongoDB
app.post(URLbase + 'logout', function(req, res) {
    var id = req.body.id; //recibimos el id del usuario
    var query = 'q={"id":' + id + ', "logged":true}';
    httpClient = requestJson.createClient(baseMlab + "/user?" + query + "&l=1&" + apiKey);
    httpClient.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1){ //encuentra el usuario con el atributo logged = true
          httpClient = requestJson.createClient(baseMlab + "/user")
          var logged = '{"$set":{"logged":false}}';
          httpClient.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(logged), function(errP, resP, bodyP) {
              //res.send({"logout":"ok", "id":body[0].id});
              res.send(body); //Response
          });
        }
        else {
          res.status(200).send('User not logged');
        }
      }
    });
});


// GET users collecion user
app.get(URLbase + 'users',
 function(request, response) {
   console.log("GET /apicol/v3/users");
   var httpClient = requestJSON.createClient(baseMlabURL);
   console.log("Cliente HTTP mLab Correcto.");
   var queryString= 'f={"_id" : 0}&';
   httpClient.get('user?' + queryString + apiKey,
      function(err, respuestaMlab, body) {
        console.log("Error: " + err);
        console.log("Respuesta Mlab: " + respuestaMlab);
        console.log("Body: " + body);
        var respuesta = {};
        respuesta = !err ? body : {"msm" : "Error al recuperar users de Mlab."};
        response.send(respuesta);
      });
});


//Petición GET con parámetros (req.params) coleccion user
/*app.get(URLbase + 'users/:id',
function (req, res) {
  console.log("GET /colapi/v3/users/:id");
  console.log(req.params.id);
  var id = req.params.id;
  var queryString = 'q={"id":' + id + '}&'
  var httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get('user?' + queryString + apiKey,
  function(err, respuestaMlab, body) {
    console.log("Error: " + err);
    console.log("Respuesta Mlab: " + respuestaMlab);
    console.log("Body: " + body);
    var respuesta = body[0];
    res.send(respuesta);
    });
});
*/

// GET users collecion account
app.get(URLbase + 'account',
 function(request, response) {
   console.log("GET /apicol/v3/users1");
   var httpClient = requestJSON.createClient(baseMlabURL);
   console.log("Cliente HTTP mLab Correcto.");
   var queryString= 'f={"_id" : 0}&';
   httpClient.get('account?' + queryString + apiKey,
      function(err, respuestaMlab, body) {
        console.log("Error: " + err);
        console.log("Respuesta Mlab: " + respuestaMlab);
        console.log("Body: " + body);
        var respuesta = {};
        respuesta = !err ? body : {"msm" : "Error al recuperar users de Mlab."};
        response.send(respuesta);
      });
});
/*//GET account a traves de mLab con parametros
app.get(URLbase + 'accounts/:id',
 function(request, response){
     var idParam = request.params.id;
     var httpClient = requestJSON.createClient(baseMLabURL);
     var queryString = 'q={"user_id":' + idParam + '}&';
     httpClient.get('account?'+ queryString + apiKeyMLab,
         function(e, respuestaMLab, body){
             var answer = body[0];
             response.send(answer);
         });
});*/


// GET users collecion MOVEMENT
app.get(URLbase + 'movement',
 function(request, response) {
   console.log("GET /apicol/v3/users2");
   var httpClient = requestJSON.createClient(baseMlabURL);
   console.log("Cliente HTTP mLab Correcto.");
   var queryString= 'f={"_id" : 0}&';
   httpClient.get('movement?' + queryString + apiKey,
      function(err, respuestaMlab, body) {
        console.log("Error: " + err);
        console.log("Respuesta Mlab: " + respuestaMlab);
        console.log("Body: " + body);
        var respuesta = {};
        respuesta = !err ? body : {"msm" : "Error al recuperar users de Mlab."};
        response.send(respuesta);
      });
});


//Petición GET con parámetros (req.params) coleccion MOVEMENT
app.get(URLbase + 'movements/:IBAN',
function (req, res) {
  var id_mov = req.params.IBAN;
  var queryString = 'q={"IBAN":' + id_mov + '}&'
  var httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get('movement?' + queryString + apiKey,
  function(err, respuestaMlab, body) {
    console.log("Respuesta Mlab: " + respuestaMlab);
    console.log("Body: " + body);
    var respuesta = body
    ;
    res.send(respuesta);
    });
});



//Petición GET con parámetros (req.params) filtro de varias colecciones. Cuentas de un usuario
app.get(URLbase + 'users/:id/accounts',
function (req, res) {
  console.log("GET /colapi/v4/users/:id/accounts");
  console.log(req.params.id);
  var id = req.params.id;
  var queryString = 'q={"userid":' + id + '}&'
  var httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get('account?' + queryString + apiKey,
  function(err, respuestaMlab, body) {
    console.log("Error: " + err);
    console.log("Respuesta Mlab: " + respuestaMlab);
    console.log("Body: " + body);
    var respuesta = body;
    res.send(respuesta);
    });
});
//q={"userid":8,"iban":456}

//Petición GET con parámetros (req.params) filtro de varias colecciones. Movimientos de una cuenta
app.get(URLbase + 'users/:id/accounts/:iban/movements',
function (req, res) {
  console.log("GET /colapi/v3/users/:id/accounts/:iban/movements");
  console.log(req.params.iban);
  var iban = req.params.iban;
  var queryString = 'q={"IBAN":' + iban + '}&'
  var httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get('movement?' + queryString + apiKey,
  function(err, respuestaMlab, body) {
    console.log("Error: " + err);
    console.log("Respuesta Mlab: " + respuestaMlab);
    console.log("Body: " + body);
    var respuesta = body;
    res.send(respuesta);
    });
});



//PUT y POST con mLab:

app.post(URLbase + 'users', function(req, res) {
 var clienteMlab = requestJSON.createClient(baseMlabURL + "/user?" + apiKey)
 clienteMlab.post('', req.body,
 function(err, resM, body) {
   res.send(body)
 });
});

app.put(URLbase + 'users/:id',
function(req, res) {
 var clienteMlab = requestJSON.createClient(baseMlabURL + "/user?" + apiKey)
 var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
 clienteMlab.put('?q={"id": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio),
 function(err, resM, body) {
   res.send(body)
 });
});

//Petición GET con parámetros (req.params) coleccion user
app.get(URLbase + 'users/:id',
function (req, res) {
  console.log("GET /colapi/v3/users/:id");
  console.log(req.params.id);
  var id = req.params.id;
  var queryString = 'q={"id":' + id + '}&'
  var httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get('user?' + queryString + apiKey,
  function(err, respuestaMlab, body) {
    console.log("Error: " + err);
    console.log("Respuesta Mlab: " + respuestaMlab);
    console.log("Body: " + body);
    var respuesta = body[0];
    res.send(respuesta);
    });
});




//POST Login con mLab
app.post(URLbase + 'login', function(req, res) {
 const httpClient = requestJSON.createClient(baseMlabURL);
 var queryString = 'q={"email":' + '"' + req.body.email + '"' + '}&';
 httpClient.get('user?' + queryString + apiKey, function(e, reqMlab, body){
   if(!body.length || !Array.isArray(body)){
     res.json({"msg":"Usuario con email errado"});
     //res.send({"msg":"Usuario con email errado"});
   }else{
     if(body[0].password !== req.body.password){
       res.json({"msg":"Usuario con clave errada"});
       //res.send({"msg":"Usuario con email errado"});
     }else{
       var login = '{"$set":{"logged":true}}';
       httpClient.put('user?q={"id":' + '"' + body[0].id + '"' + '}&' + apiKey, JSON.parse(login), function(e, reqMlab, body) {
             //res.send({"login":"ok"});
             res.status(200);
             //res.json({"msg":"Usuario OK"});
         });
     }
   }
 });
 httpClient.get('user?' + queryString + apiKey, function(e, reqMlab, body){
     console.log(body);
     res.send(body);
 });
 res.status(200);
});












//LOGIN con MLAB
app.post(URLbase + 'login',
  function (req, res) {
    console.log("POST Correcto");
    console.log(req.body.email);
    console.log(req.body.password);
    var email=req.body.email;
    var pass = req.body.password;
    var clienteMlab = requestJSON.createClient(baseMlabURL);// + "/user?" + apiKey)
    var queryString = 'q={"email":"' + email + '"}&q={"password":"' + pass + '"}&f={"_id" : 0}&';
      clienteMlab.get('user?' + queryString + apiKey,
        function (err, respmlab, body){
          var resp=body[0];
          console.log(body);
              if (body.length > 0){
                body.logged=true;
                var cambio = '{"$set":' + JSON.stringify(body) + '}'
                clienteMlab.put('?q={"email": "' + req.body.email + '"}&' + apiKey, JSON.parse(cambio),
                function(err, resM, body) {
                  res.send(body)
                });
              }
          res.send(resp);
          });
  });

  //LOGOUT con MLAB
  app.post(URLbase + 'login',
    function (req, res) {
      console.log("POST Correcto");
      console.log(req.body.email);
      console.log(req.body.password);
      var email=req.body.email;
      var pass = req.body.password;
      var clienteMlab = requestJSON.createClient(baseMlabURL);// + "/user?" + apiKey)
      var queryString = 'q={"email":"' + email + '"}&q={"password":"' + pass + '"}&f={"_id" : 0}&';
        clienteMlab.get('user?' + queryString + apiKey,
          function (err, respmlab, body){
            var resp=body[0];
            console.log(body);
                if (body.length > 0){
                  delete us.logged;
                  var cambio = '{"$set":' + JSON.stringify(body) + '}'
                  clienteMlab.put('?q={"email": "' + req.body.email + '"}&' + apiKey, JSON.parse(cambio),
                  function(err, resM, body) {
                    res.send(body)
                  });
                }
            res.send(resp);
            });
    });




//Login y logout esta funciòn actualizà el campo de logued con el valor de loginState(true o false)
var update_User_state = function(req, resp, loginState) {
  console.log(loginState);
  var email = req.body.email;
  var password = req.body.password;
  var queryString='?q={"email":"'+ email+'"}&';
  //se consulta cliente por correo
  client.get(MLAB + 'users'+ queryString + apikeyQuery , function(err, res, body) {
    var usuario = body[0];
    //si es un logout o si el password es el almacenado
    if(!loginState || usuario.password == password){
      usuario.loged = loginState;
    }
    //se actualiza usuario con el estado del login/logout
    client.put(MLAB + '/users/'+ usuario._id.$oid + apikey, usuario, function(err, res, body) {
      console.log(res.statusCode);
      resp.json(res);
    });
  });
};
































































//LOGIN con archivo
app.post(URLbase + 'login',
  function (req, res) {
    console.log("POST Correcto");
    console.log(req.body.email);
    console.log(req.body.paswword);
    var user=req.body.email;
    var pass = req.body.paswword;
    for (us of usersFile){
      if(us.email == user) {
        if(us.paswword == pass) {
          us.logged = true;
          console.log('Login Correcto');
          writeUserDataToFile(usersFile);
          res.send({"msg" : "Login Correcto."});
        } else {
          console.log('Login Incorrecto');
          res.send({"msg" : "Login Incorrecto."});
          }
      } else {
      console.log('Email Incorrecto');
      res.send({"msg" : "Email Incorrecto."});
      }
    }
  }
);

//LOGOUT con archivo
app.post(URLbase + 'logout',
  function (req, res) {
    console.log("POST Correcto");
    console.log(req.body.email);
    console.log(req.body.paswword);
    var user=req.body.email;
    var pass = req.body.paswword;
    for (us of usersFile){
      if(us.email == user) {
        if(us.paswword == pass) {
          delete us.logged;
          writeUserDataToFile(usersFile);
          console.log('Login Correcto');
          res.send({"msg" : "Login Correcto."});
        }
        else {
          console.log('Login Incorrecto');
          res.send({"msg" : "Login Incorrecto."});
              }
            }
            else{
            console.log('Email Incorrecto');
            res.send({"msg" : "Email Incorrecto."});
          }
        }
    }
);


//Función para persistir cambios en users.json
function writeUserDataToFile(data) {
 var fs = require('fs');
 var jsonUserData = JSON.stringify(data);

 fs.writeFile("./users2.json", jsonUserData, "utf8",
  function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
      console.log(err);
    } else {
      console.log("Datos escritos en 'users2.json'.");
    }
  })
}


































app.get('/colapi/v1',
  function (req, res) {
    console.log("GET /colapi/v1");
    console.log(req.params);
    console.log(req.headers);
    res.send({"msg" : "Hola Colombia Practitioner BBVA 2018 Christian!"});
});

app.post('/colapi/v1',
  function (req, res) {
    res.send('Probando el POST');
});

app.put('/colapi/v1',
  function (req, res) {
    res.send('Probando el PUT');
});

app.delete('/colapi/v1',
  function (req, res) {
    res.send('Probando el DELETE');
});

app.get('/colapi/v1/users',
  function (req, res) {
    console.log("GET /colapi/v1/users");
    res.sendfile('./users.json');
});

app.get('/colapi/v1/users1/:id',
  function (req, res) {
    console.log("GET /colapi/v1/users1/:id");
    var pos = req.params.id;
    res.send(usersFile[pos - 1]);
});

//peticion get con parametro
app.get(URLbase + 'users2/:id',
  function (req, res) {
    console.log("GET /colapi/v1/users2/:id");
    var pos = req.params.id;
    res.send(usersFile[pos - 1]);
});

//peticion get con query string (req.query)
app.get(URLbase + 'users3',
  function (req, res) {
    console.log("GET con query string.");
    console.log(req.query);
    res.send({"msg" : "GET con query string."});
});

//peticion POST
app.post(URLbase + 'users4',
  function (req, res) {
    console.log("POST Correcto");
    res.send({"christian" : "diaz"});
});

//peticion POST
app.post(URLbase + 'users5',
  function (req, res) {
    console.log("POST Correcto");
    res.sendfile('users.json');
});

//peticion POST
app.post(URLbase + 'users6',
  function (req, res) {
    console.log("POST Correcto");
    res.sendfile('users.json',{root: __dirname});
});

//peticion POST por body en postman, new ID coge el ultimo
app.post(URLbase + 'users7',
  function (req, res) {
    console.log("POST Correcto");
    console.log(req.body);
    console.log(req.body.first_name);
    var jsonID=req.body;
    var newID = usersFile.length + 1;
    jsonID.id = newID;
    console.log(newID);
    usersFile.push(req.body);
    console.log(usersFile);
    res.send({"msg" : "Usuario creado correctamente.", jsonID});
});






// GET users
app.get(URLbase + 'users',
 function(request, response) {
   console.log(URLbase);
   console.log(usersFile);
   response.send(usersFile);
});

// Petición GET con parámetros (req.params)
app.get(URLbase + 'users/:id/:otro',
 function (req, res) {
   console.log("GET /colapi/v2/users/:id/:otro");
   console.log(req.params);
   console.log('req.params.id: ' + req.params.id);
   console.log('req.params.otro: ' + req.params.otro);
   var respuesta = req.params;
   res.send(respuesta);
});

// Petición GET con Query String (req.query)
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET con query string.");
   console.log(req.query.id);
   console.log(req.query.country);
   res.send(usersFile[pos - 1]);
   respuesta.send({"msg" : "GET con query string"});
});

// Petición POST (reg.body)
app.post(URLbase + 'users',
 function(req, res) {
   var newID = usersFile.length + 1;
   var newUser = {
     "id" : newID,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "country" : req.body.country
   };
   usersFile.push(newUser);
   console.log(usersFile);
   res.send({"msg" : "Usuario creado correctamente: ", newUser});
 });




 // Peticion PUT
 app.put(URLbase + 'users/:id',
   function(req, res){
     console.log("PUT /colapi/v2/users/:id");
     var idBuscar = req.params.id;
     var updateUser = req.body;
     for(i = 0; i < usersFile.length; i++) {
       console.log(usersFile[i].id);
       if(usersFile[i].id == idBuscar) {
         res.send({"msg" : "Usuario actualizado correctamente.", "id" : idBuscar, updateUser});
       }
     }
     res.send({"msg" : "Usuario no encontrado.", updateUser});
   });



   // Peticion DELETE
   app.delete(URLbase + 'users/:id',
     function(req, res){
       console.log("DELETE /colapi/v2/users/:id");
       var idBorrar = req.params.id;
       for(i = 0; i < usersFile.length; i++) {
         console.log(usersFile[i].id);
         if(usersFile[i].id == idBorrar) {
           res.send({"msg" : "Usuario eliminado correctamente.", "id" : idBorrar});
         }
       }
       res.send({"msg" : "Usuario no encontrado."});
     });




     // Peticion DELETE con splice
     app.delete(URLbase + 'users/:id',
       function(req, res){
         console.log("DELETE /colapi/v2/users/:id");
         var idBorrar = req.params.id;
         var deleteUser = req.body;
         usersFile.splice(id - 1, 1)

         for(i = 0; i < usersFile.length; i++) {
           console.log(usersFile[i].id);
           if(usersFile[i].id == idBorrar) {
             res.send({"msg" : "Usuario eliminado correctamente.", "id" : idBorrar, deleteUser});
           }
         }
         res.send({"msg" : "Usuario no encontrado.", deleteUser});
       });




       // PUT
       app.put(URLbase + 'users/:id',
         function(req, res){
           console.log("PUT /colapi/v2/users/:id");
           var idBuscar = req.params.id;
           var updateUser = req.body;
           var encontrado = false;
           for(i = 0; (i < usersFile.length) && !encontrado; i++) {
             console.log(usersFile[i].id);
             if(usersFile[i].id == idBuscar) {
               encontrado = true;
               res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
             }
           }
           if(!encontrado) {
             res.send({"msg" : "Usuario no encontrado.", updateUser});
           }
         });

       // DELETE
       app.delete(URLbase + 'users/:id',
        function(req, res) {

          const id = req.params.id-1;
          const reg = usersFile[id];

          if(undefined != reg){
            usersFile.splice(id,1);
            res.send(204);
         } else
           res.send(404);
       });



       //PUT y POST con mLab:
       app.post('/v3/users', function(req, res) {
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
        clienteMlab.post('', req.body, function(err, resM, body) {
          res.send(body)
        })
      });

       app.put('/v3/usuarios/:id', function(req, res) {
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
        clienteMlab.put('?q={"idusuario": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
          res.send(body)
        })
      });







       //LOGIN
       app.post(URLbase + 'login',
         function (req, res) {
           console.log("POST Correcto");
           console.log(req.body.email);
           console.log(req.body.paswword);
           var user=req.body.email;
           var pass = req.body.paswword;
           for (us of usersFile){
             if(us.email == user) {
               if(us.paswword == pass) {
                 us.logged = true;
                 console.log('Login Correcto');
                 writeUserDataToFile(usersFile);
                 res.send({"msg" : "Login Correcto."});
               } else {
                 console.log('Login Incorrecto');
                 res.send({"msg" : "Login Incorrecto."});
                 }
             } else {
             console.log('Email Incorrecto');
             res.send({"msg" : "Email Incorrecto."});
             }
           }
         }
       );
