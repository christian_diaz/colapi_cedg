FROM node

#Se define el directorio del contenedor
WORKDIR /Borrador_Proyecto_Chris_mLab

#Adiciona el contenido del proyecto en el directorio del contenedor
ADD . /Borrador_Proyecto_Chris_mLab

#Puerto por el que escucha el contenedor
EXPOSE 3000

#Comandos para lanzar nuestra API REST 'colapi'
CMD ["npm", "start"]
