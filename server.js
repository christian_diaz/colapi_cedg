var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./users2.json');
var urlBase = "/colapi/v4/";
var baseMlab = "https://api.mlab.com/api/1/databases/colapidb_cedg/collections/";
var apiKey = "apiKey=8I89WCD5GQOSapHVuElAZYj-TTFNZc9R";

app.listen(port);
app.use(bodyParser.json());
console.log("colapi escuchando en puerto " + port + "...");

//login desde polymer a mLab ***************************************************

//POST login usando API mLab para conexion con MongoDB
app.post(urlBase + 'login', function(req, res) {
    var email = req.body.email //Variable con el email
    var password = req.body.password //Var con el password
    var query = 'q={"email":"' + email + '","password":"' + password + '"}' //queryString: se utiliza para formar la URL de consulta para mLab
    clientMlab = requestJson.createClient(baseMlab + "/user?" + query + "&l=1&" + apiKey); //Peticion a mLab
    //console.log(baseMlab + "/user?" + query + "&l=1&" + apiKey);
    clientMlab.get('', function(err, resM, body) { //Consultamos si existe usuario
      if (!err) {
        if (body.length == 1){ // Si encuentra el usuario
          clientMlab = requestJson.createClient(baseMlab + "/user");
          var logged = '{"$set":{"logged":true}}'; //Con el set agregamos el estado logged
          clientMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(logged), function(errP, resP, bodyP) { //PUT agregar logged
              //console.log(body);
              res.send(body); //Response
          });
        }
        else {
          res.status(404).send('User not found'); //No hay resultados
        }
      }
    });
});

//POST Login v1 alternativo con mLab
app.post(urlBase + 'login8', function(req, res) {
 const httpClient = requestJson.createClient(baseMlab);
 var queryString = 'q={"email":' + '"' + req.body.email + '"' + '}&';
 httpClient.get('user?' + queryString + apiKey, function(e, reqMlab, body){
   if(!body.length || !Array.isArray(body)){
     res.json({"msg":"Error email"});
   }else{
     if(body[0].password !== req.body.password){
       res.json({"msg":"Error password"});
     }else{
       var login = '{"$set":{"logged":true}}';
       httpClient.put('user?q={"id":' + '"' + body[0].id + '"' + '}&' + apiKey, JSON.parse(login), function(e, reqMlab, body) {
             res.status(200).send(body);
         });
     }
   }
 });
});

//POST Login con mLab proyecto final
app.post(urlBase + 'login5', function(req, res) {
 const httpClient = requestJson.createClient(baseMlab);
 var queryString = 'q={"email":' + '"' + req.body.email + '"' + '}&';
 httpClient.get('user?' + queryString + apiKey, function(e, reqMlab, body){
   if(!body.length || !Array.isArray(body)){
     res.json({"msg":"Usuario con email errado"});
     //res.send({"msg":"Usuario con email errado"});
   }else{
     if(body[0].password !== req.body.password){
       res.json({"msg":"Usuario con clave errada"});
       //res.send({"msg":"Usuario con email errado"});
     }else{
       var login = '{"$set":{"logged":true}}';
       httpClient.put('user?q={"id":' + '"' + body[0].id + '"' + '}&' + apiKey, JSON.parse(login), function(e, reqMlab, body) {
             //res.send({"login":"ok"});
             res.status(200);
             //res.json({"msg":"Usuario OK"});
         });
     }
   }
 });
 httpClient.get('user?' + queryString + apiKey, function(e, reqMlab, body){
     console.log(body);
     res.send(body);
 });
 res.status(200);
});



//Petición GET con parámetros (req.params) coleccion account proyecto final
app.get(urlBase + 'accounts/:id',
function (req, res) {
  //console.log("GET /colapi/v3/users1/:userid");
  console.log(req.params.id);
  var userId = req.params.id;
  var queryString = 'q={"userid":' + userId + '}&'
  var httpClient = requestJson.createClient(baseMlab);
  httpClient.get('account?' + queryString + apiKey,
  function(err, respuestaMlab, body) {
    //console.log("Error: " + err);
    console.log("Respuesta Mlab: " + respuestaMlab);
    console.log("Body: " + body);
    var respuesta = body;
    res.send(respuesta);
    });
});


//Petición GET con parámetros (req.params) coleccion MOVEMENT proyecto final
app.get(urlBase + 'movements/:IBAN',
function (req, res) {
  var id_mov = req.params.IBAN;
  var queryString = 'q={"IBAN":' + id_mov + '}&'
  var httpClient = requestJson.createClient(baseMlab);
  httpClient.get('movement?' + queryString + apiKey,
  function(err, respuestaMlab, body) {
    console.log("Respuesta Mlab: " + respuestaMlab);
    console.log("Body: " + body);
    var respuesta = body
    ;
    res.send(respuesta);
    });
});


//POST logout usando API mLab para conexion con MongoDB
app.post(urlBase + 'logout', function(req, res) {
    var id = req.body.id; //recibimos el id del usuario
    var query = 'q={"id":' + id + ', "logged":true}';
    httpClient = requestJson.createClient(baseMlab + "/user?" + query + "&l=1&" + apiKey);
    httpClient.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1){ //encuentra el usuario con el atributo logged = true
          httpClient = requestJson.createClient(baseMlab + "/user")
          var logged = '{"$set":{"logged":false}}';
          httpClient.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(logged), function(errP, resP, bodyP) {
              //res.send({"logout":"ok", "id":body[0].id});
              res.send(body); //Response
          });
        }
        else {
          res.status(200).send('User not logged');
        }
      }
    });
});

// GET users collecion user
app.get(urlBase + 'users',
 function(request, response) {
   console.log("GET /apicol/v3/users");
   var httpClient = requestJson.createClient(baseMlab);
   console.log("Cliente HTTP mLab Correcto.");
   var queryString= 'f={"_id" : 0}&';
   httpClient.get('user?' + queryString + apiKey,
      function(err, respuestaMlab, body) {
        console.log("Error: " + err);
        console.log("Respuesta Mlab: " + respuestaMlab);
        console.log("Body: " + body);
        var respuesta = {};
        respuesta = !err ? body : {"msm" : "Error al recuperar users de Mlab."};
        response.send(respuesta);
      });
});
